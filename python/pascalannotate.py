import Tkinter
import pascalraw as p
from PIL import ImageDraw, Image, ImageTk
import sys
import os

#Global Constants
VALID_IMAGE_FORMATS = ['.png', '.jpg']
VALID_OBJECT_CLASSES = ['car', 'person', 'bicycle']
VALID_OBJECT_POSES = ['Unspecified', 'Left', 'Right', 'Frontal', 'Rear']
BOUNDING_BOX_COLOR = 'green'
TEXT_COLOR = 'green'
TEXT_ANCHOR = 'nw'

if len(sys.argv) >=5:
    inputPath = sys.argv[1] 
    outputPath = sys.argv[2]
else:
    ignoreAnnotatedImages = raw_input('Ignore images that have already been annotated? (y/n): ')
    inputPath = raw_input('Please enter path to images folder: ')
    outputPath = raw_input('Please enter path for output annotations: ')
    folderName = raw_input('Please enter parent folder name of images and annotations subdirectories (e.g. VOC2014): ')

#clean up paths. add trailing backslash in case it doesn't already exist.
inputPath = os.path.normpath(inputPath) + os.sep
outputPath = os.path.normpath(outputPath) + os.sep

imageFiles = []
for currentFile in os.listdir(inputPath):
    fileName, fileExtension = os.path.splitext(currentFile)
    if (fileExtension.lower() in VALID_IMAGE_FORMATS):
        if (ignoreAnnotatedImages.lower() == 'y'):
            if not os.path.isfile(os.path.join(outputPath, fileName + '.xml')):
                imageFiles.append(os.path.join(inputPath, currentFile))
        else:
            imageFiles.append(os.path.join(inputPath, currentFile))

def writeXmlAnnotation(outputPath, imageFile, image, boundingBoxList):  
    #calculate imageHeight, imageWidth, and imageDepth
    imageWidth = image.size[0]
    imageHeight = image.size[1]
    if image.mode in ['1', 'L', 'P', 'I', 'F']:
        imageDepth = 1
    elif image.mode in ['RGB', 'YCbCr']:
        imageDepth = 3
    elif image.mode in ['RGBA', 'CMYK']:
        imageDepth = 4
    else:
        print 'Error: Unsupported image mode. Image depth cannot be determined.'
        return

    fileName, fileExtension = os.path.splitext(os.path.basename(imageFile))
    outputFile = os.path.join(outputPath, fileName + '.xml')
    f = open(outputFile, 'w')
    f.write('<annotation>\n')
    f.write('\t<folder>' + folderName + '</folder>\n')
    f.write('\t<filename>' + fileName + fileExtension + '</filename>\n')
    f.write('\t<source>\n')
    f.write('\t\t<database>PASCAL VOC Compatible Annotation Database</database>\n')
    f.write('\t\t<annotation>Department of Electrical Engineering</annotation>\n')
    f.write('\t\t<image>Stanford University</image>\n')
    f.write('\t</source>\n')
    f.write('\t<segmented>0</segmented>\n')
    for box in boundingBoxList:
        rectangle = box.box
        truncated = box.truncated
        occluded = box.occluded
        difficult = box.difficult
        objectClass = box.objectClass
        objectPose = box.objectPose
        #convert coordinates from Tkinter format ((0,0) origin) to PASCAL format ((1,1) origin)
        coordinates = [int(coordinates) + 1 for coordinates in canvas.coords(rectangle)]
        f.write('\t<object>\n')
        f.write('\t\t<name>' + objectClass + '</name>\n')
        f.write('\t\t<bndbox>\n')
        f.write('\t\t\t<xmax>' + str(coordinates[2]) + '</xmax>\n')
        f.write('\t\t\t<xmin>' + str(coordinates[0]) + '</xmin>\n')
        f.write('\t\t\t<ymax>' + str(coordinates[3]) + '</ymax>\n')
        f.write('\t\t\t<ymin>' + str(coordinates[1]) + '</ymin>\n')
        f.write('\t\t</bndbox>\n')
        f.write('\t\t<difficult>' + str(difficult) + '</difficult>\n')
        f.write('\t\t<occluded>' + str(occluded) + '</occluded>\n')
        f.write('\t\t<pose>' + objectPose + '</pose>\n')
        f.write('\t\t<truncated>' + str(truncated) + '</truncated>\n')
        f.write('\t</object>\n')
    f.write('\t<size>\n')
    f.write('\t\t<depth>' + str(imageDepth) + '</depth>\n')
    f.write('\t\t<height>' + str(imageHeight) + '</height>\n')
    f.write('\t\t<width>' + str(imageWidth) + '</width>\n')
    f.write('\t</size>\n')
    f.write('</annotation>\n')
    f.close()

for imageFile in imageFiles:
    image = Image.open(imageFile)

    #Local Constants
    IMAGE_WIDTH = image.size[0]
    IMAGE_HEIGHT = image.size[1]

    window = Tkinter.Tk(className="pascal annotator")
    #make sure window is always opened in the same location
    window.geometry('+0+0')
    canvas = Tkinter.Canvas(window, width=image.size[0], height=image.size[1], bd=0, highlightthickness=0)
    canvas.pack()

    class TkBoundingBox(p.BoundingBox):
        def __init__(self, initialX1=0, initialY1=0, initialX2=0, initialY2=0, initialTruncated=0, initialOccluded=0, initialDifficult=0, initialObjectClass='', initialObjectPose=''):
            super(self.__class__, self).__init__(initialX1, initialY1, initialX2, initialY2, initialTruncated, initialOccluded, initialDifficult, initialObjectClass, initialObjectPose)
            #add a single space in front of the objectClass for aesthetics. 
            self.classLabel = canvas.create_text(self.x1, self.y1, text=' ' + self.objectClass, fill=TEXT_COLOR, anchor=TEXT_ANCHOR)
            self.box = canvas.create_rectangle(self.x1, self.y1, self.x2, self.y2, outline=BOUNDING_BOX_COLOR)

        def clipX(self, x):
            """clips x coordinate values such that they are within the image"""
            if x < 0:
                return 0
            elif x >= IMAGE_WIDTH:
                return IMAGE_WIDTH - 1
            else:
                return x

        def clipY(self, y):
            """clips y coordinate values such that they are within the image"""
            if y < 0:
                return 0
            elif y >= IMAGE_HEIGHT:
                return IMAGE_HEIGHT - 1
            else:
                return y

        def setCorner(self, event):
            self.x1 = self.clipX(event.x)
            self.y1 = self.clipY(event.y)
            self.x2 = self.clipX(event.x)
            self.y2 = self.clipY(event.y)
            self.truncated = objectTruncated.get()
            self.occluded = objectOccluded.get()
            self.difficult = objectDifficult.get()
            self.objectClass = selectedObjectClass.get()
            self.objectPose = selectedObjectPose.get()
            canvas.delete(self.box)
            self.box = canvas.create_rectangle(self.x1, self.y1, self.x2, self.y2, outline=BOUNDING_BOX_COLOR)
            canvas.delete(self.classLabel)
            #add a single space in front of the objectClass for aesthetics. 
            self.classLabel = canvas.create_text(self.x1, self.y1, text=' ' + self.objectClass, fill=TEXT_COLOR, anchor=TEXT_ANCHOR)

        def motion(self, event):
            self.x2 = self.clipX(event.x)
            self.y2 = self.clipY(event.y)
            self.truncated = objectTruncated.get()
            self.occluded = objectOccluded.get()
            self.difficult = objectDifficult.get()
            self.objectClass = selectedObjectClass.get()
            self.objectPose = selectedObjectPose.get()
            canvas.delete(self.box)
            self.box = canvas.create_rectangle(self.x1, self.y1, self.x2, self.y2, outline=BOUNDING_BOX_COLOR)
            canvas.delete(self.classLabel)
            #add a single space in front of the objectClass for aesthetics. 
            self.classLabel = canvas.create_text(self.x1, self.y1, text=' ' + self.objectClass, fill=TEXT_COLOR, anchor=TEXT_ANCHOR)

    class TkBoundingBoxList(object):
        def __init__(self):
            #list of TkBoundingBox objects
            self.boxes = []

        def addTkBoundingBox(self, boundingBox):
            #only add boxes with non-zero area, since <ButtonRelease-1> is triggered on the first mouse click (not release)
            coordinates = canvas.coords(boundingBox.box)
            canvas.delete(boundingBox.box)
            canvas.delete(boundingBox.classLabel)
            if not ((coordinates[0] == coordinates[2]) and (coordinates[1] == coordinates[3])):
                box = TkBoundingBox(boundingBox.x1, boundingBox.y1, boundingBox.x2, boundingBox.y2, boundingBox.truncated, boundingBox.occluded, boundingBox.difficult, boundingBox.objectClass, boundingBox.objectPose)
                self.boxes.append(box)

        def getTkBoundingBoxList(self):
            return self.boxes

        def clear(self):
            for box in self.boxes:
                rectangle = box.box
                canvas.delete(rectangle)
                label = box.classLabel
                canvas.delete(label)
            self.boxes = []

        def clearLastTkBoundingBox(self):
            if self.boxes:
                lastBox = self.boxes.pop()
                lastRectangle = lastBox.box
                canvas.delete(lastRectangle)
                lastLabel = lastBox.classLabel
                canvas.delete(lastLabel)

    def addTkBoundingBox(event):
        myBoxList.addTkBoundingBox(myBox)

    def done(root):
        writeXmlAnnotation(outputPath, imageFile, image, myBoxList.getTkBoundingBoxList())
        root.destroy()

    myBox = TkBoundingBox()
    myBoxList = TkBoundingBoxList()   

    clearButton = Tkinter.Button(window, text="clear", command=myBoxList.clear)
    clearButton.pack(side='left', expand='true')
    undoButton = Tkinter.Button(window, text="undo", command=myBoxList.clearLastTkBoundingBox)
    undoButton.pack(side='left', expand='true')
    doneButton = Tkinter.Button(window, text="done", command=lambda root=window:done(window))
    doneButton.pack(side='left', expand='true')
    selectedObjectClass = Tkinter.StringVar(window)
    selectedObjectClass.set(VALID_OBJECT_CLASSES[0]) # default value
    objectClassMenu = apply(Tkinter.OptionMenu, (window, selectedObjectClass) + tuple(VALID_OBJECT_CLASSES))
    objectClassMenu.pack(side='left', expand='true')
    selectedObjectPose = Tkinter.StringVar(window)
    selectedObjectPose.set(VALID_OBJECT_POSES[0]) # default value
    objectPoseMenu = apply(Tkinter.OptionMenu, (window, selectedObjectPose) + tuple(VALID_OBJECT_POSES))
    objectPoseMenu.pack(side='left', expand='true')
    objectTruncated = Tkinter.IntVar()
    truncatedCheckButton = Tkinter.Checkbutton(window, text="truncated", variable=objectTruncated)
    truncatedCheckButton.pack(side='left', expand='true')
    objectOccluded = Tkinter.IntVar()
    occludedCheckButton = Tkinter.Checkbutton(window, text="occluded", variable=objectOccluded)
    occludedCheckButton.pack(side='left', expand='true')
    objectDifficult = Tkinter.IntVar()
    difficultCheckButton = Tkinter.Checkbutton(window, text="difficult", variable=objectDifficult)
    difficultCheckButton.pack(side='left', expand='true')

    image_tk = ImageTk.PhotoImage(image)
    canvas.create_image(0, 0, image=image_tk, anchor='nw')

    fileName, fileExtension = os.path.splitext(os.path.basename(imageFile))
    if os.path.isfile(os.path.join(outputPath, fileName + '.xml')):
        boundingBoxList = p.getBoundingBoxes(os.path.join(outputPath, fileName + '.xml'))
        for boundingBox in boundingBoxList:
            #convert coordinates from PASCAL format ((1,1) origin) to Tkinter format ((0,0) origin)
            myBox = TkBoundingBox(boundingBox.x1 - 1, boundingBox.y1 - 1, boundingBox.x2 - 1, boundingBox.y2 - 1, boundingBox.truncated, boundingBox.occluded, boundingBox.difficult, boundingBox.objectClass, boundingBox.objectPose)
            myBoxList.addTkBoundingBox(myBox)

    canvas.bind("<Button-1>", myBox.setCorner)
    canvas.bind("<B1-Motion>", myBox.motion)
    canvas.bind("<ButtonRelease-1>", addTkBoundingBox)
    Tkinter.mainloop()
