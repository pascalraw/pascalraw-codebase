function dummyVar = downsamplePgmToPng(args)
    HORIZONTAL_CROP_LEFT = 16;
    HORIZONTAL_CROP_RIGHT = 64;
    VERTICAL_CROP_TOP = 6;
    VERTICAL_CROP_BOTTOM = 6;

    files = dir(strcat(args.pgmPath,'*.pgm'));
    for i = 1:numel(files)
        [pathstr,name,ext] = fileparts(files(i).name);
        filename = files(i).name;
        %only downsample the file if it has not already been downsampled.
        if exist(strcat(args.pngPath, name, '.png'))
            fprintf(strcat(filename, ' (downsampled previously)\n'));
        else
            fprintf(strcat(filename, '\n'));
            im = imread(strcat(args.pgmPath, filename));
            imDimensions = size(im);
            imHeight = imDimensions(1);
            imWidth = imDimensions(2);
            if imWidth < imHeight
                im = im(1+HORIZONTAL_CROP_LEFT:end-HORIZONTAL_CROP_RIGHT, 1+VERTICAL_CROP_TOP:end-VERTICAL_CROP_BOTTOM);
            else
                im = im(1+VERTICAL_CROP_TOP:end-VERTICAL_CROP_BOTTOM, 1+HORIZONTAL_CROP_LEFT:end-HORIZONTAL_CROP_RIGHT);
            end
            if args.downsamplingFactor > 1
                im = blkproc(im, [args.downsamplingFactor args.downsamplingFactor], 'mean2');
            end
            im = uint16(im);
            imwrite(im, strcat(args.pngPath, name, '.png'));
        end
    end
    dummyVar = 0;
end
